#!/bin/bash

# Find out where we're running from
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ $# -eq 1 ]; then
	DOCKERFILE=$1
else
	echo "Need name of Dockerfile"
	exit 1
fi

if [ ! -f "${DOCKERFILE}" ]; then
  echo "** File ${DOCKERFILE} not found."
  echo "Execute this script from within ${script_dir}"
  exit 1
fi

# shellcheck disable=SC1090
source "${script_dir}/repo_config.txt"

cp "${DOCKERFILE}" Dockerfile

# build a local image
# shellcheck disable=SC2154
docker build --rm -t "${image}" .

rm Dockerfile
